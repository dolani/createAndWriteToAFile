﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace writefile
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] fruits = {"Cashew", "Mango", "Banana", "Apple"};
            String path = @"C:\Users\USER\Desktop\fruits.txt";

            File.WriteAllLines(path, fruits);
        }
    }
}
